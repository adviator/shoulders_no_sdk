package com.programsdesigningfor.shoulders;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class WorkoutActivity extends AppCompatActivity {

    public int idWorkout;
    DataBaseHelper dbHelper;
    Boolean isSubscribeChecked = false;	// Проверялась ли подписка

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        /* -------------------------------------------------
           Работа с базой данных
         ------------------------------------------------- */

        // Получаем идентификатор выбранной страницы
        Intent intent = getIntent();
        idWorkout = intent.getIntExtra("id", 1);

        // Получаем данные из БД о текущей странице
        dbHelper = new DataBaseHelper(this);
        ArrayList<String> workout = dbHelper.getCurrentWorkout(idWorkout);
        String title = workout.get(0);
        String text = workout.get(1);

        // Получаем тренировочную программу, привязанную к этой тренировке
        ArrayList<ArrayList> training = dbHelper.getTraining(idWorkout);
        ArrayList<Integer> exercise_id = training.get(0);
        ArrayList<String> exercise_image = training.get(1);
        ArrayList<String> exercise_title = training.get(2);
        ArrayList<String> exercise_set = training.get(3);
        ArrayList<String> exercise_repeat = training.get(4);

        int n = exercise_title.size();
        for (int i = 0; i < n; i++) {
            addRow(exercise_id.get(i), exercise_image.get(i), exercise_title.get(i), exercise_set.get(i), exercise_repeat.get(i));
        }

        // Получаем тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Кнопка Назад
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                Intent intent = new Intent(WorkoutActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        // Заголовок тренировки
        getSupportActionBar().setTitle(title);

        TextView mText = (TextView) findViewById(R.id.workout_text);
        mText.setText(text);

        // Запрещаем поворот экрана
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Вычисляем высоту устройства в dp
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        final float dpHeight = displayMetrics.heightPixels / displayMetrics.density;

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
//
//        Log.i("LOG", "isSubscribeChecked - " + isSubscribeChecked);
//
//        // Запуск рекламы
//        runAd();
//
//        // Простая проверка подписки
//        runCheckSubscribe();
//
//        // Если не было проверки подписки то запустить и подписать
//        if (!isSubscribeChecked) {
//            runSubscribeTimeout();
//            isSubscribeChecked = true;
//        }

        final ScrollView scrollView = (ScrollView) findViewById(R.id.viewPager);
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) scrollView.getLayoutParams();

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                // Вызывается если баннер загрузился

                // В зависимости от высоты устройства меняем отступ для баннера
                if (dpHeight <= 400) params.bottomMargin = Math.round(32 * displayMetrics.density);
                if (dpHeight > 400 && dpHeight <= 720)
                    params.bottomMargin = Math.round(50 * displayMetrics.density);
                if (dpHeight > 720) params.bottomMargin = Math.round(90 * displayMetrics.density);

                scrollView.setLayoutParams(params);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Вызывается, когда не удается выполнить запрос. Возможные коды ошибок:
                //AdRequest.ERROR_CODE_INTERNAL_ERROR
                //AdRequest.ERROR_CODE_INVALID_REQUEST
                //AdRequest.ERROR_CODE_NETWORK_ERROR
                //AdRequest.ERROR_CODE_NO_FILL

                // Баннер не загрузился, ставим нулевой отступ
                params.bottomMargin = 0;
                scrollView.setLayoutParams(params);
            }

        });
    }


    public void addRow(final int id, String img, String title, String set, String repeat) {

        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) findViewById(R.id.training_table);

        // Создаём экземпляр инфлейтера, который понадобится для создания строки таблицы из шаблона. В качестве контекста у нас используется сама активити
        LayoutInflater inflater = LayoutInflater.from(this);

        // Создаём строку таблицы, используя шаблон из файла /res/layout/row_training.xml
        final ViewGroup nullParent = null;
        TableRow tr = (TableRow) inflater.inflate(R.layout.row_training, nullParent);

        // Находим картинку для упражнения
        ImageView image_row = (ImageView) tr.findViewById(R.id.exercise_image);
        // Устанавливаем картинку
        Integer resId = getResources().getIdentifier(img, "drawable", getPackageName());
        image_row.setImageResource(resId);

        // Находим ячейку для упражнения
        TextView exercise_row = (TextView) tr.findViewById(R.id.exercise_title);
        // Устанавливаем текст
        exercise_row.setText(title);

        // Находим ячейку для сетов
        TextView set_row = (TextView) tr.findViewById(R.id.exercise_set);
        // Устанавливаем текст
        set_row.setText(set);

        // Находим ячейку для повторений
        TextView repeat_row = (TextView) tr.findViewById(R.id.exercise_repeat);
        // Устанавливаем текст
        repeat_row.setText(repeat);

        // Добавляем созданную строку в таблицу
        tableLayout.addView(tr);

        tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WorkoutActivity.this, ExerciseActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }


    // Создаём меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.appbar_workout, menu);
//        return super.onCreateOptionsMenu(menu);

        // Меню Отключить рекламму
//        MenuItem disableAdItem = menu.findItem(R.id.disableAd);
//        if (isSubscribedFromPrefs() && disableAdItem != null) {
//            disableAdItem.setVisible(false);
//        }

        return true;
    }


    // Обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getTitle().equals("Убрать рекламму")) {
//            runSubscribe();
//        }

        switch (item.getItemId()) {
            case R.id.action_edit:
                editWorkout(); // Редактирование тренировки
                break;
            case R.id.action_delete:
                deleteWorkout(); // Удаление тренировки
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }


    // Удаляем текущую тренировку
    void deleteWorkout() {
        // Создаём диалоговое окно для подтверждения удаления
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        // Заголовок окна
        adb.setTitle(R.string.workout_delete);
        // Иконка для красоты
        adb.setIcon(android.R.drawable.ic_dialog_info);
        // Сообщение
        adb.setMessage(R.string.delete_msg);
        // Да, удаляем тренировку
        adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Удаляем тренировку
                dbHelper.deleteWorkout(idWorkout);
                // Выводим сообщение, что тренировка удалена
                Toast toast = Toast.makeText(getApplicationContext(), R.string.workout_remove, Toast.LENGTH_SHORT);
                toast.show();

                // Закрываем активность
                finish();
            }
        });
        // Отменяем удаление
        adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        // Создаём диалог и показываем его
        adb.create().show();
    }


    // Редактируем текущую тренировку
    void editWorkout() {
        Intent intent = new Intent(WorkoutActivity.this, WorkoutEditActivity.class);
        intent.putExtra("id", idWorkout);
        startActivity(intent);
    }
}
