package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity {

    Boolean isSubscribeChecked = false;	// Проверялась ли подписка

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Запрещаем поворот экрана
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /* -------------------------------------------------
           Определяем параметры устройства и показываем
           рекламу соответственно им.
        ------------------------------------------------- */

        // Вычисляем высоту устройства в dp
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        final float dpHeight = displayMetrics.heightPixels / displayMetrics.density;

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

//        Log.i("LOG", "isSubscribeChecked - " + isSubscribeChecked);
//
//        // Запуск рекламы
//        runAd();
//
//        // Простая проверка подписки
//        runCheckSubscribe();
//
//        // Если не было проверки подписки то запустить и подписать
//        if (!isSubscribeChecked) {
//            runSubscribeTimeout();
//            isSubscribeChecked = true;
//        }

        final LinearLayout adLayout = (LinearLayout) findViewById(R.id.viewPager);
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) adLayout.getLayoutParams();

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                // Вызывается, если баннер загрузился

                // В зависимости от высоты устройства меняем отступ для баннера
                if (dpHeight <= 400) params.bottomMargin = Math.round(32 * displayMetrics.density);
                if (dpHeight > 400 && dpHeight <= 720)
                    params.bottomMargin = Math.round(50 * displayMetrics.density);
                if (dpHeight > 720) params.bottomMargin = Math.round(90 * displayMetrics.density);

                adLayout.setLayoutParams(params);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Баннер не загрузился, ставим нулевой отступ
                params.bottomMargin = 0;
                adLayout.setLayoutParams(params);
            }
        });

        /* ------------------------------------------------- */

        // Выводим тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        /* -------------------------------------------------
           Создание вкладок
        ------------------------------------------------- */

        // Создаём вкладки
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        final TabLayout.Tab tabWorkout = tabLayout.newTab();
        final TabLayout.Tab tabExercise = tabLayout.newTab();

        // Выводим иконки и текст во вкладках
        final ViewGroup parent = null;

        // Вкладка Упражнения
        View exerciseTabView = getLayoutInflater().inflate(R.layout.tab_view, parent);
        ImageView tabIcon = (ImageView) exerciseTabView.findViewById(R.id.tabIcon);
        TextView tabText = (TextView) exerciseTabView.findViewById(R.id.tabText);
        tabIcon.setImageResource(R.drawable.ic_tab_exercise);
        tabText.setText(R.string.tab_exercise);

        // Вкладка Тренировки
        View workoutTabView = getLayoutInflater().inflate(R.layout.tab_view, parent);
        tabIcon = (ImageView) workoutTabView.findViewById(R.id.tabIcon);
        tabText = (TextView) workoutTabView.findViewById(R.id.tabText);
        tabIcon.setImageResource(R.drawable.ic_tab_workout);
        tabText.setText(R.string.tab_workout);

        // Устанавливаем вид вкладок
        tabExercise.setCustomView(exerciseTabView);
        tabWorkout.setCustomView(workoutTabView);

        // Добавляем вкладки
        tabLayout.addTab(tabWorkout);
        tabLayout.addTab(tabExercise);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    // Создаём меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.appbar_main, menu);
//        return super.onCreateOptionsMenu(menu);

//        // Меню Отключить рекламму
//        MenuItem disableAdItem = menu.findItem(R.id.disableAd);
//        if (isSubscribedFromPrefs() && disableAdItem != null) {
//            disableAdItem.setVisible(false);
//        }

        return true;
    }

    // Обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getTitle().equals("Убрать рекламму")) {
//            runSubscribe();
//        }

        switch (item.getItemId()) {
            case R.id.action_generator:
                // Переходим к генератору тренировок
                Intent intent = new Intent(MainActivity.this, GeneratorActivity.class);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }
}


class PagerAdapter extends FragmentPagerAdapter {

    // Число вкладок
    private static final int NUM_TABS = 2;

    PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new TabWorkout();
            case 1:
                return new TabExercise();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_TABS;
    }

}


