package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

// -----------------------------------------------
// Создаём вкладку для упражнений
// и выводим список всех упражнений
// с картинками и наванием
// -----------------------------------------------

public class TabExercise extends Fragment {

    View view;
    ListView listExercise;
    private Parcelable state = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_exercise, container, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        CreateListOfExercise(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        CreateListOfExercise(view);
        if (state != null)
            listExercise.onRestoreInstanceState(state);
    }

    @Override
    public void onPause() {
        super.onPause();
        state = listExercise.onSaveInstanceState();
    }


    @SuppressWarnings("unchecked")
    void CreateListOfExercise(View view) {
        // Подключаем БД
        DataBaseHelper dbHelper = new DataBaseHelper(getContext());

        // Находим список для заполнения
        listExercise = (ListView) view.findViewById(R.id.listExercise);

        // Получаем все упражнения из БД
        ArrayList<ArrayList> exercise = dbHelper.getAllExercise();
        final ArrayList<Integer> exercise_id = exercise.get(0);
        ArrayList<String> exercise_title = exercise.get(1);
        ArrayList<String> exercise_image = exercise.get(2);
        ArrayList<Integer> exercise_favorite = exercise.get(3);

        // Заполняем список данными
        final CustomListViewAdapter adapter = new CustomListViewAdapter(getActivity(), exercise_title, exercise_image, exercise_favorite);
        listExercise.setAdapter(adapter);

        // Нажали на произвольный пункт списка
        listExercise.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TabExercise.this.getActivity(), ExerciseActivity.class);
                intent.putExtra("id", exercise_id.get(position));
                startActivity(intent);
            }
        });

    }

}
