package com.programsdesigningfor.shoulders;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class CustomListViewAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList<String> mListText = new ArrayList<>();
    private ArrayList<String> mListImage = new ArrayList<>();
    private ArrayList<Integer> mListFavorite = new ArrayList<>();

    CustomListViewAdapter(Context mContext, ArrayList<String> mListText, ArrayList<String> mListImage, ArrayList<Integer> mListFavorite) {
        super(mContext, R.layout.exercise_item, mListText);
        this.mContext = mContext;
        this.mListText = mListText;
        this.mListImage = mListImage;
        this.mListFavorite = mListFavorite;
    }

    @Override
    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            //LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //convertView = inflater.inflate(R.layout.exercise_item, parent, false);
            //convertView = LayoutInflater.from(mContext).inflate(R.layout.exercise_item, parent, false);
            convertView = View.inflate(mContext, R.layout.exercise_item, null);

            holder = new ViewHolder();

            // Получаем текстовое поле
            holder.textView = (TextView) convertView.findViewById(R.id.list_text);

            // Получаем поле для изображения
            holder.imageView = (ImageView) convertView.findViewById(R.id.list_image);

            // Получаем поле для избранного
            holder.favoriteView = (ImageView) convertView.findViewById(R.id.list_favorite);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // Устанавливаем текст
        holder.textView.setText(mListText.get(position));

        // Выделяем избранные упражнения
        if (mListFavorite.get(position) == 1)
            holder.favoriteView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
        else
            holder.favoriteView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimaryLight));

        // Запоминаем позицию пункта списка
        holder.position = position;

        // По имени картинки получаем ссылку на ресурс
        holder.resId = mContext.getResources().getIdentifier(mListImage.get(position), "drawable", mContext.getPackageName());

        // Устанавливаем картинку
        new LoadImageTask().execute(holder);

        //holder.imageView.setImageResource(resId);
        // Преобразуем картинку упражнения в битмап
        //Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), resId);
        // Масштабируем до размера 70x70 dp
        //Bitmap exercise_bitmap = Bitmap.createScaledBitmap(bitmap, convertDpToPixel(70), convertDpToPixel(70), false);
        // Устанавливаем картинку
        //holder.imageView.setImageBitmap(exercise_bitmap);

        return convertView;
    }

    // Преобразуем dp в пиксели
    private int convertDpToPixel(float dp) {
        Resources resources = mContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }


    // Сохраняем текст и картинки
    private static class ViewHolder {
        TextView textView;
        ImageView imageView;
        ImageView favoriteView;
        int position;
        Integer resId;
    }


    private class LoadImageTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            return params[0];
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            //Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), result.resId);
            //result.imageView.setImageBitmap(bitmap);
            //result.imageView.setImageResource(result.resId);
            // Преобразуем картинку упражнения в битмап
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), result.resId);
            // Масштабируем до размера 70x70 dp
            Bitmap exercise_bitmap = Bitmap.createScaledBitmap(bitmap, convertDpToPixel(70), convertDpToPixel(70), false);
            // Устанавливаем картинку
            result.imageView.setImageBitmap(exercise_bitmap);
        }
    }

}



