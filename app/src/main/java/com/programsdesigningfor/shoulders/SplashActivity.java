package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        // Создаём БД
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            throw new Error("Не могу скопировать БД");
        }

        // Открываем БД
        try {
            dbHelper.openDataBase();
        } catch (SQLException e) {
            throw new Error("Не могу открыть БД");
        }

        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
        overridePendingTransition(0, R.anim.splash_fade_out);

    }
}
