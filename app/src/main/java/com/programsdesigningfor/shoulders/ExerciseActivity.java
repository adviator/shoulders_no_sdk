package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class ExerciseActivity extends AppCompatActivity {

    int id; // Идентификатор упражнения
    boolean setFavorite = false; // Добавлено упражнение в избранное или нет
    Boolean isSubscribeChecked = false;	// Проверялась ли подписка

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        // Запрещаем поворот экрана
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /* -------------------------------------------------
           Определяем параметры устройства и показываем
           рекламу соответственно им.
         ------------------------------------------------- */

        // Вычисляем высоту устройства в dp
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        final float dpHeight = displayMetrics.heightPixels / displayMetrics.density;

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

//        Log.i("LOG", "isSubscribeChecked - " + isSubscribeChecked);
//
//        // Запуск рекламы
//        runAd();
//
//        // Простая проверка подписки
//        runCheckSubscribe();
//
//        // Если не было проверки подписки то запустить и подписать
//        if (!isSubscribeChecked) {
//            runSubscribeTimeout();
//            isSubscribeChecked = true;
//        }

        final CustomScrollView scrollView = (CustomScrollView) findViewById(R.id.viewPager);
        final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) scrollView.getLayoutParams();

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                // Вызывается, если баннер загрузился

                // В зависимости от высоты устройства меняем отступ для баннера
                if (dpHeight <= 400) params.bottomMargin = Math.round(32 * displayMetrics.density);
                if (dpHeight > 400 && dpHeight <= 720)
                    params.bottomMargin = Math.round(50 * displayMetrics.density);
                if (dpHeight > 720) params.bottomMargin = Math.round(90 * displayMetrics.density);

                scrollView.setLayoutParams(params);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Вызывается, когда не удается выполнить запрос. Возможные коды ошибок:
                //AdRequest.ERROR_CODE_INTERNAL_ERROR
                //AdRequest.ERROR_CODE_INVALID_REQUEST
                //AdRequest.ERROR_CODE_NETWORK_ERROR
                //AdRequest.ERROR_CODE_NO_FILL

                // Баннер не загрузился, ставим нулевой отступ
                params.bottomMargin = 0;
                scrollView.setLayoutParams(params);
            }

        });

        // Получаем ID выбранного упражнения
        Intent intent = getIntent();
        id = intent.getIntExtra("id", 1);

        /* -------------------------------------------------
           Работа с базой данных
         ------------------------------------------------- */

        // Получаем данные из БД о текущей странице
        final DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<String> exercise = dbHelper.getCurrentExercise(id);
        String title = exercise.get(0);
        String text = exercise.get(1);
        String img1 = exercise.get(2);
        String img2 = exercise.get(3);
        String tips = exercise.get(4);
        int favorite = Integer.parseInt(exercise.get(5));

        // Получаем тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Кнопка Назад
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Заголовок упражнения
        getSupportActionBar().setTitle(title);

        // Описание упражнения
        TextView mText = (TextView) findViewById(R.id.exercise_text);
        mText.setText(text);

        // Фотографии упражнения
        ImageView mImg1 = (ImageView) findViewById(R.id.exercise_img1);
        Integer resId = getResources().getIdentifier(img1, "drawable", getPackageName());
        mImg1.setImageResource(resId);
        ImageView mImg2 = (ImageView) findViewById(R.id.exercise_img2);
        resId = getResources().getIdentifier(img2, "drawable", getPackageName());
        mImg2.setImageResource(resId);

        // Советы
        if (tips != null) {
            TextView mTips = (TextView) findViewById(R.id.exercise_tips);
            mTips.setText(tips);
        } else {
            // Для данного упражнения советов нет, прячем заголовок
            TextView mTipsTitle = (TextView) findViewById(R.id.exercise_tips_title);
            mTipsTitle.setVisibility(View.GONE);
        }


        /* -------------------------------------------------
           Избранные упражнения
         ------------------------------------------------- */

        // Добавляем кнопку для избранного
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        // Ставим соответствующую иконку
        if (fab != null) {
            setFavorite = (favorite == 1);

            // Ставим соответствующую иконку
            if (setFavorite) fab.setImageResource(R.drawable.ic_favorite);
            else fab.setImageResource(R.drawable.ic_favorite_border);

            // Щелчок по кнопке для избранного (fab)
            fab.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Страница уже содержится в Избранном
                    if (setFavorite) {
                        // Меняем вид иконки
                        fab.setImageResource(R.drawable.ic_favorite_border);
                        // Удаляем из Избранного
                        dbHelper.ExerciseFavorite(id, 0);
                        // Выводим сообщение, что страница удалена из Избранного
                        Toast toast = Toast.makeText(getApplicationContext(),
                                R.string.favorite_remove, Toast.LENGTH_SHORT);
                        toast.show();
                        setFavorite = false;
                    } else {
                        // Меняем вид иконки
                        fab.setImageResource(R.drawable.ic_favorite);
                        // Добавляем в Избранное
                        dbHelper.ExerciseFavorite(id, 1);
                        // Выводим сообщение, что страница добавлена в Избранное
                        Toast toast = Toast.makeText(getApplicationContext(),
                                R.string.favorite_add, Toast.LENGTH_SHORT);
                        toast.show();
                        setFavorite = true;
                    }
                }
            });


            // При прокручивании вниз кнопка для избранного прячется
            scrollView.setOnScrollViewListener(new CustomScrollView.OnScrollViewListener() {
                public void onScrollChanged(CustomScrollView v, int l, int t, int oldl, int oldt) {
                    //Log.d("SCROLL", "t = "+t+", l = "+l);
                    if (t > 0) {
                        fab.hide();
                    } else {
                        fab.show();
                    }
                }
            });

        }
    }

}

