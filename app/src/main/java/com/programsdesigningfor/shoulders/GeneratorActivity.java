package com.programsdesigningfor.shoulders;

import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioGroup;

public class GeneratorActivity extends AppCompatActivity {

    RadioGroup radioGroupTarget;
    public static final String KEY_TARGET = "TARGET";
    SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generator);

        // Получаем тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Кнопка Назад
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Заголовок
        getSupportActionBar().setTitle(R.string.generator);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        GeneratorPreference generator = new GeneratorPreference();
        transaction.add(R.id.fragment, generator);
        transaction.commit();
    }


    // Переходим к самому генератору тренировок
    public void generator(View view) {
        radioGroupTarget = (RadioGroup) findViewById(R.id.target);
        int targetIndex = radioGroupTarget.indexOfChild(findViewById(radioGroupTarget.getCheckedRadioButtonId()));

        /* -------------------------------------------------
           Запоминаем данные
        ------------------------------------------------- */
        mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(KEY_TARGET, targetIndex);
        editor.apply();

        /* -------------------------------------------------
           Данные для следующего фрагмента
        ------------------------------------------------- */
        Bundle bundle = new Bundle();
        bundle.putInt("target", targetIndex);
        GeneratorWorkout generator = new GeneratorWorkout();
        generator.setArguments(bundle);

        /* -------------------------------------------------
           Заменяем фрагмент
        ------------------------------------------------- */
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment, generator)
                .addToBackStack("FragmentB")
                .commit();
    }

}



