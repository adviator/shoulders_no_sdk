package com.programsdesigningfor.shoulders;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WorkoutEditActivity extends AppCompatActivity {

    public int idWorkout; // Идентификатор тренировки
    DataBaseHelper dbHelper;
    static final int maxRow = 6; // Максимальное число строк упражнений
    int numRow; // Текущее число строк упражнений
    int rowId = 0; // Идентификатор строки
    SparseArray<ArrayList> training = new SparseArray<>();


    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_edit);

        // -------------------------------------------------
        //   Тулбар
        // -------------------------------------------------

        // Получаем тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Кнопка Назад
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Заголовок тренировки
        getSupportActionBar().setTitle(R.string.workout_edit);

        // -------------------------------------------------
        //   Работа с БД
        // -------------------------------------------------

        // Получаем идентификатор выбранной тренировки
        Intent intent = getIntent();
        idWorkout = intent.getIntExtra("id", 1);

        dbHelper = new DataBaseHelper(this);
        ArrayList<String> workout = dbHelper.getCurrentWorkout(idWorkout);
        String title = workout.get(0); // Заголовок тренировки
        String text = workout.get(1); // Описание тренировки

        // Заполняем поля
        EditText mTitle = (EditText) findViewById(R.id.title);
        mTitle.setText(title);
        EditText mDescription = (EditText) findViewById(R.id.description);
        mDescription.setText(text);

        // Получаем тренировочную программу, привязанную к этой тренировке
        ArrayList<ArrayList> exercise = dbHelper.getTraining(idWorkout);
        final ArrayList<Integer> exercise_id = exercise.get(0);
        ArrayList<String> exercise_image = exercise.get(1);
        ArrayList<String> exercise_title = exercise.get(2);
        final ArrayList<String> exercise_set = exercise.get(3);
        final ArrayList<String> exercise_repeat = exercise.get(4);

        for (int i = 0, n = exercise_id.size(); i < n; i++) {
            addExercise(exercise_id.get(i), exercise_image.get(i), exercise_title.get(i), exercise_set.get(i), exercise_repeat.get(i));
            numRow++; // Подсчитываем число строк
            rowId++;
        }
        // Число упражнений превышает максимум
        if (numRow >= maxRow) {
            // Делаем кнопку "Сохранить" серой
            ImageView addButton = (ImageView) findViewById(R.id.add_row);
            addButton.setColorFilter(ContextCompat.getColor(this, R.color.colorDivider));
        }
    }


    // Нажали на кнопку Отмена, возвращаемся обратно
    public void onCancelClick(View view) {
        Intent intent = new Intent(this, WorkoutActivity.class);
        intent.putExtra("id", idWorkout);
        startActivity(intent);
    }


    // Сохраняем отредактированную тренировку
    public void onSaveClick(View view) {
        // Получаем заголовок
        EditText mTitle = (EditText) findViewById(R.id.title);
        String title = mTitle.getText().toString();
        // Проверяем, не пустой ли заголовок
        if (title.equals("")) title = getResources().getString(R.string.workout_my);

        // Получаем описание тренировки
        EditText mDescription = (EditText) findViewById(R.id.description);
        String description = mDescription.getText().toString();

        // Сохраняем тренировку в базе данных
        dbHelper.updateWorkout(idWorkout, title, description);

        // Сохраняем упражнения для этой тренировки
        dbHelper.updateTraining(idWorkout, training);

        // Выводим сообщение, что тренировка сохранена
        Toast toast = Toast.makeText(getApplicationContext(), R.string.workout_save, Toast.LENGTH_SHORT);
        toast.show();

        // Возвращаемся обратно к списку тренировок
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    // Добавление упражнения
    @SuppressWarnings("unchecked")
    public void addExercise(final int id, String img, String title, final String set, final String repeat) {
        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) findViewById(R.id.training_table);

        // Создаём экземпляр инфлейтера, который понадобится для создания строки таблицы из шаблона. В качестве контекста у нас используется сама активити
        LayoutInflater inflater = LayoutInflater.from(this);

        // Создаём строку таблицы, используя шаблон из файла /res/layout/row_training.xml
        final ViewGroup nullParent = null;
        TableRow tr = (TableRow) inflater.inflate(R.layout.row_training_edit, nullParent);

        // Для строки таблицы ставим уникальный идентификатор
        tr.setTag(rowId);

        // Находим картинку для упражнения
        ImageView image_row = (ImageView) tr.findViewById(R.id.exercise_image);
        // Устанавливаем картинку
        Integer resId = getResources().getIdentifier(img, "drawable", getPackageName());
        image_row.setImageResource(resId);

        // Находим ячейку для упражнения
        TextView exercise_row = (TextView) tr.findViewById(R.id.exercise_title);
        // Устанавливаем текст
        exercise_row.setText(title);

        // Находим ячейку для сетов
        final EditText set_row = (EditText) tr.findViewById(R.id.exercise_set);
        // Устанавливаем текст
        set_row.setText(set);

        // Отслеживаем изменение текста
        set_row.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    // Получаем идентификатор строки
                    TableRow tr = (TableRow) set_row.getParent();
                    final int idE = (int) tr.getTag();

                    // Получаем данные текущего упражнения
                    ArrayList group = training.get(idE);
                    // Меняем число подходов
                    group.set(1, s.toString());
                    // Сохраняем выбранное упражнение обратно
                    training.put(idE, group);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        // Находим ячейку для повторений
        final EditText repeat_row = (EditText) tr.findViewById(R.id.exercise_repeat);
        // Устанавливаем текст
        repeat_row.setText(repeat);

        // Отслеживаем изменение текста
        repeat_row.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // Получаем идентификатор строки
                TableRow tr = (TableRow) repeat_row.getParent();
                final int idE = (int) tr.getTag();

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем число повторов
                group.set(2, s.toString());
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        // Добавляем созданную строку в таблицу
        tableLayout.addView(tr);

        // Добавляем в таблицу данные
        ArrayList group = new ArrayList() {
            {
                add(id); // Идентификатор упражнения
                add(set); // Подходы
                add(repeat); // Повторения
            }
        };

        training.put(rowId, group);
    }


    public void newExercise(View view) {
        // Разблокируем кнопку "Сохранить"
        if (numRow == 0) {
            Button save = (Button) findViewById(R.id.saveWorkout);
            save.setEnabled(true);
        }
        // Пока строк меньше максимального значения
        if (numRow < maxRow) {
            // Добавляем новую строку
            newRow();
            numRow++; // Увеличиваем счётчик строк

            ImageView addButton = (ImageView) view.findViewById(R.id.add_row);
            addButton.requestFocus(); // Переносим фокус к кнопке

            if (numRow >= maxRow) {
                // Делаем кнопку "Сохранить" серой
                addButton.setColorFilter(ContextCompat.getColor(this, R.color.colorDivider));
            }
        }
    }


    // Удаление строки
    public void deleteRow(View view) {
        // Находим родителя для выбранной строки
        TableRow tr = (TableRow) view.getParent();
        int id = (int) tr.getTag();
        training.delete(id);
        // Удаляем строку
        tr.removeAllViews();
        // Уменьшаем число строк
        numRow--;
        // Возвращаем цвет кнопке
        ImageView addButton = (ImageView) findViewById(R.id.add_row);
        addButton.clearColorFilter();
        addButton.requestFocus(); // Переносим фокус к кнопке
        // Блокируем кнопку "Сохранить"
        if (numRow == 0) {
            Button save = (Button) findViewById(R.id.saveWorkout);
            save.setEnabled(false);
        }
    }


    @SuppressWarnings("unchecked")
    public void newRow() {
        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) findViewById(R.id.training_table);

        // Создаём экземпляр инфлейтера, который понадобится для создания строки таблицы из шаблона. В качестве контекста у нас используется сама активити
        LayoutInflater inflater = LayoutInflater.from(this);

        // Создаём строку таблицы, используя шаблон из файла /res/layout/row_training_add.xml
        final ViewGroup nullParent = null;
        TableRow tr = (TableRow) inflater.inflate(R.layout.row_training_edit, nullParent);

        // Для строки таблицы ставим уникальный идентификатор
        tr.setTag(rowId);

        // Добавляем в таблицу данные
        ArrayList group = new ArrayList() {
            {
                add(11); // Идентификатор упражнения
                add("5"); // Подходы
                add("10"); // Повторения
            }
        };

        // Ставим подходы
        final EditText exercise_set = (EditText) tr.findViewById(R.id.exercise_set);

        exercise_set.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    // Получаем идентификатор строки
                    TableRow tr = (TableRow) exercise_set.getParent();
                    final int idE = (int) tr.getTag();

                    // Получаем данные текущего упражнения
                    ArrayList group = training.get(idE);
                    // Меняем число подходов
                    group.set(1, s.toString());
                    // Сохраняем выбранное упражнение обратно
                    training.put(idE, group);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        // Ставим повторения
        final EditText exercise_repeat = (EditText) tr.findViewById(R.id.exercise_repeat);
        exercise_repeat.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // Получаем идентификатор строки
                TableRow tr = (TableRow) exercise_repeat.getParent();
                final int idE = (int) tr.getTag();

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем число повторов
                group.set(2, s.toString());
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        training.put(rowId, group);
        rowId++; // Увеличиваем идентификатор

        // Добавляем созданную строку в таблицу
        tableLayout.addView(tr);
    }


    // Выбор упражнения из списка
    @SuppressWarnings("unchecked")
    public void selectExercise(View view) {
        final TextView text = (TextView) view.findViewById(R.id.exercise_title);
        final ImageView img = (ImageView) view.findViewById(R.id.exercise_image);

        TableRow tr = (TableRow) view.getParent();
        final int idE = (int) tr.getTag();

        // Создаём диалог
        final Dialog dialog = new Dialog(this);
        // Макет для вывода списка
        dialog.setContentView(R.layout.tab_exercise);

        // Подключаем БД
        DataBaseHelper dbHelper = new DataBaseHelper(this);

        // Получаем все упражнения из БД
        ArrayList<ArrayList> exercise = dbHelper.getAllExercise();
        final ArrayList<Integer> exercise_id = exercise.get(0);
        final ArrayList<String> exercise_title = exercise.get(1);
        final ArrayList<String> exercise_image = exercise.get(2);
        ArrayList<Integer> exercise_favorite = exercise.get(3);

        // Находим список для заполнения
        ListView listExercise = (ListView) dialog.findViewById(R.id.listExercise);

        // Заполняем список данными
        final CustomListViewAdapter adapter = new CustomListViewAdapter(this, exercise_title, exercise_image, exercise_favorite);
        listExercise.setAdapter(adapter);

        // Нажали на произвольный пункт списка
        listExercise.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Подставляем текст для выбранного упражнения
                text.setText(exercise_title.get(position));
                // По имени картинки получаем ссылку на ресурс
                Integer resId = getResources().getIdentifier(exercise_image.get(position), "drawable", getPackageName());
                // Устанавливаем картинку
                //img.setImageResource(resId);
                // Преобразуем картинку упражнения в битмап
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resId);
                // Масштабируем до размера 70x70 dp
                Bitmap exercise_bitmap = Bitmap.createScaledBitmap(bitmap, convertDpToPixel(70), convertDpToPixel(70), false);
                // Устанавливаем картинку
                img.setImageBitmap(exercise_bitmap);

                // Закрываем диалог
                dialog.dismiss();

                ImageView addButton = (ImageView) findViewById(R.id.add_row);
                addButton.requestFocus(); // Переносим фокус к кнопке

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем идентификатор упражнения
                group.set(0, exercise_id.get(position));
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }
        });

        // Ставим заголовок
        dialog.setTitle(R.string.exercise_all);
        // Показываем диалог
        dialog.show();
    }


    // Преобразуем dp в пиксели
    private int convertDpToPixel(float dp) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

}
