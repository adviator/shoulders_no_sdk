package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class TabWorkout extends Fragment implements AdapterView.OnItemSelectedListener {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_workout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        CreateListOfWorkout(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        CreateListOfWorkout(view);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @SuppressWarnings("unchecked")
    void CreateListOfWorkout(View view) {
        // Подключаем БД
        DataBaseHelper dbHelper = new DataBaseHelper(getContext());

        // Находим список для заполнения
        GridView listWorkout = (GridView) view.findViewById(R.id.listWorkout);

        // Получаем все мышцы из БД
        ArrayList<ArrayList> workout = dbHelper.getAllWorkout();
        ArrayList<String> workout_text = workout.get(0);
        ArrayList<String> workout_image = workout.get(1);
        final ArrayList<Integer> workout_id = workout.get(2);

        // Заполняем список данными
        final CustomGridViewAdapter adapter = new CustomGridViewAdapter(getActivity(), workout_text, workout_image);
        listWorkout.setAdapter(adapter);

        listWorkout.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(TabWorkout.this.getActivity(), WorkoutActivity.class);
                intent.putExtra("id", workout_id.get(position));
                startActivity(intent);

            }
        });

        // Добавляем кнопку для новой тренировки
        final FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        if (fab != null) {
            // Создаём новую тренировку
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TabWorkout.this.getActivity(), WorkoutNewActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
