package com.programsdesigningfor.shoulders;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class GeneratorPreference extends Fragment {

    RadioGroup radioGroupTarget;
    public static final String KEY_TARGET = "TARGET";
    SharedPreferences mSettings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_generator_preference, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        /* -------------------------------------------------
           Получаем значения переключателей из настроек
        ------------------------------------------------- */
        mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (mSettings.contains(KEY_TARGET)) {
            int targetIndex = mSettings.getInt(KEY_TARGET, 1);
            radioGroupTarget = (RadioGroup) view.findViewById(R.id.target);
            ((RadioButton) radioGroupTarget.getChildAt(targetIndex)).setChecked(true);
        }
    }

}
