package com.programsdesigningfor.shoulders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class CustomGridViewAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList<String> mGridText = new ArrayList<>();
    private ArrayList<String> mGridImage = new ArrayList<>();

    CustomGridViewAdapter(Context mContext, ArrayList<String> mGridText, ArrayList<String> mGridImage) {
        super(mContext, R.layout.workout_item, mGridText);
        this.mContext = mContext;
        this.mGridText = mGridText;
        this.mGridImage = mGridImage;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.workout_item, parent, false);

            holder = new ViewHolder();

            // Получаем текстовое поле
            holder.textView = (TextView) convertView.findViewById(R.id.grid_text);

            // Получаем поле для изображения
            holder.imageView = (ImageView) convertView.findViewById(R.id.grid_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // Устанавливаем текст
        holder.textView.setText(mGridText.get(position));
        // По имени картинки получаем ссылку на ресурс
        Integer resId = mContext.getResources().getIdentifier(mGridImage.get(position), "drawable", mContext.getPackageName());
        // Устанавливаем картинку
        holder.imageView.setImageResource(resId);

        return convertView;
    }

    private static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }

}

