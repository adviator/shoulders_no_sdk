package com.programsdesigningfor.shoulders;

import android.app.Dialog;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WorkoutNewActivity extends AppCompatActivity {

    static final String[] data_set = {"2", "3", "4", "5", "6"};
    static final String[] data_repeat = {"4-6", "6-8", "8-10", "10-12", "12-15"};
    static final int maxRow = 6; // Максимальное число строк упражнений
    int numRow = 1; // Текущее число строк упражнений
    int rowId = 0; // Идентификатор строки
    SparseArray<ArrayList> training = new SparseArray<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_new);

        // Получаем тулбар
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Кнопка Назад
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Заголовок тренировки
        getSupportActionBar().setTitle(R.string.workout_new);

        // Сразу же вставляем новую строку
        newRow();
    }


    // Нажали на кнопку Отмена, возвращаемся обратно
    public void onCancelClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    // Сохраняем тренировку
    public void onSaveClick(View view) {
        // Подключаем БД
        DataBaseHelper dbHelper = new DataBaseHelper(this);

        // Получаем заголовок
        EditText mTitle = (EditText) findViewById(R.id.title);
        String title = mTitle.getText().toString();
        // Проверяем, не пустой ли заголовок
        if (title.equals("")) title = getResources().getString(R.string.workout_my);

        // Получаем описание тренировки
        EditText mDescription = (EditText) findViewById(R.id.description);
        String description = mDescription.getText().toString();

        // Сохраняем тренировку в базе данных
        long lastId = dbHelper.newWorkout(title, description);

        // Сохраняем упражнения для этой тренировки
        dbHelper.newTraining(lastId, training);

        // Выводим сообщение, что новая тренировка добавлена
        Toast toast = Toast.makeText(getApplicationContext(), R.string.workout_add, Toast.LENGTH_SHORT);
        toast.show();

        // Возвращаемся обратно к списку тренировок
        Intent intent = new Intent(WorkoutNewActivity.this, MainActivity.class);
        startActivity(intent);
    }


    // Добавление новой строки
    public void addRow(View view) {
        // Разблокируем кнопку Сохранить
        if (numRow == 0) {
            Button save = (Button) findViewById(R.id.saveWorkout);
            save.setEnabled(true);
        }
        // Пока строк меньше максимального значения
        if (numRow < maxRow) {
            // Добавляем новую строку
            newRow();
            numRow++; // Увеличиваем счётчик строк

            if (numRow >= maxRow) {
                // Делаем кнопку серой
                ImageView addButton = (ImageView) view.findViewById(R.id.add_row);
                addButton.setColorFilter(ContextCompat.getColor(this, R.color.colorDivider));
            }
        }
    }


    // Удаление строки
    public void deleteRow(View view) {
        // Находим родителя для выбранной строки
        TableRow tr = (TableRow) view.getParent();
        int id = (int) tr.getTag();
        training.delete(id);
        // Удаляем строку
        tr.removeAllViews();
        // Уменьшаем число строк
        numRow--;
        // Возвращаем цвет кнопке
        ImageView addButton = (ImageView) findViewById(R.id.add_row);
        addButton.clearColorFilter();
        // Блокируем кнопку Сохранить
        if (numRow == 0) {
            Button save = (Button) findViewById(R.id.saveWorkout);
            save.setEnabled(false);
        }
    }


    @SuppressWarnings("unchecked")
    public void newRow() {
        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) findViewById(R.id.training_table);

        // Создаём экземпляр инфлейтера, который понадобится для создания строки таблицы из шаблона. В качестве контекста у нас используется сама активити
        LayoutInflater inflater = LayoutInflater.from(this);

        // Создаём строку таблицы, используя шаблон из файла /res/layout/row_training_add.xml
        final ViewGroup nullParent = null;
        TableRow tr = (TableRow) inflater.inflate(R.layout.row_training_add, nullParent);

        // Для строки таблицы ставим уникальный идентификатор
        tr.setTag(rowId);

        // Ставим подходы
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data_set);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner exercise_set = (Spinner) tr.findViewById(R.id.exercise_set);
        exercise_set.setAdapter(adapter);
        exercise_set.setSelection(3);

        // Пользователь поменял значение подходов
        exercise_set.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {
                // Получаем идентификатор строки
                TableRow tr = (TableRow) parent.getParent();
                final int idE = (int) tr.getTag();

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем число подходов
                group.set(1, data_set[selectedItemPosition]);
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Ставим повторения
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data_repeat);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner exercise_repeat = (Spinner) tr.findViewById(R.id.exercise_repeat);
        exercise_repeat.setAdapter(adapter);
        exercise_repeat.setSelection(2);

        // Пользователь поменял значение повторений
        exercise_repeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {
                // Получаем идентификатор строки
                TableRow tr = (TableRow) parent.getParent();
                final int idE = (int) tr.getTag();

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем число повторов
                group.set(2, data_repeat[selectedItemPosition]);
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Добавляем созданную строку в таблицу
        tableLayout.addView(tr);

        // Добавляем в таблицу данные
        ArrayList group = new ArrayList() {
            {
                add(11); // Идентификатор упражнения
                add(data_set[3]); // Подходы
                add(data_repeat[2]); // Повторения
            }
        };

        training.put(rowId, group);
        rowId++; // Увеличиваем идентификатор
    }


    // Выбор упражнения из списка
    @SuppressWarnings("unchecked")
    public void selectExercise(View view) {

        final TextView text = (TextView) view.findViewById(R.id.exercise_title);
        final ImageView img = (ImageView) view.findViewById(R.id.exercise_image);

        TableRow tr = (TableRow) view.getParent();
        final int idE = (int) tr.getTag();

        // Создаём диалог
        final Dialog dialog = new Dialog(this);
        // Макет для вывода списка
        dialog.setContentView(R.layout.tab_exercise);

        // Подключаем БД
        DataBaseHelper dbHelper = new DataBaseHelper(this);

        // Получаем все упражнения из БД
        ArrayList<ArrayList> exercise = dbHelper.getAllExercise();
        final ArrayList<Integer> exercise_id = exercise.get(0);
        final ArrayList<String> exercise_title = exercise.get(1);
        final ArrayList<String> exercise_image = exercise.get(2);
        ArrayList<Integer> exercise_favorite = exercise.get(3);

        // Находим список для заполнения
        ListView listExercise = (ListView) dialog.findViewById(R.id.listExercise);

        // Заполняем список данными
        final CustomListViewAdapter adapter = new CustomListViewAdapter(this, exercise_title, exercise_image, exercise_favorite);
        listExercise.setAdapter(adapter);

        // Нажали на произвольный пункт списка
        listExercise.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Подставляем текст для выбранного упражнения
                text.setText(exercise_title.get(position));
                // По имени картинки получаем ссылку на ресурс
                Integer resId = getResources().getIdentifier(exercise_image.get(position), "drawable", getPackageName());
                // Устанавливаем картинку
                img.setImageResource(resId);
                // Закрываем диалог
                dialog.dismiss();

                // Получаем данные текущего упражнения
                ArrayList group = training.get(idE);
                // Меняем идентификатор упражнения
                group.set(0, exercise_id.get(position));
                // Сохраняем выбранное упражнение обратно
                training.put(idE, group);
            }
        });

        // Ставим заголовок
        dialog.setTitle(R.string.exercise_all);
        // Показываем диалог
        dialog.show();
    }

}
