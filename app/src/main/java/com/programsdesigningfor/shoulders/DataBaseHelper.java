package com.programsdesigningfor.shoulders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

class DataBaseHelper extends SQLiteOpenHelper {

    private SQLiteDatabase mDataBase;
    private final Context mContext;
    private static String DB_PATH = "";
    private static String DB_NAME = "shoulders.db";
    private static final int DB_VERSION = 1;

    DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;

        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
            //DB_PATH = mContext.getDatabasePath(DB_NAME).getPath();
        }
    }

    // Создаём пустую базу данных и перезаписываем её нашей собственной базой
    void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            // Cоздаём пустую базу, позже она будет перезаписана
            this.getReadableDatabase();
            try {
                // Копируем БД из assets
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Ошибка копирования базы данных");
            }
            this.close();
        }
    }

    // Копирование БД из assets в папку приложения
    private void copyDataBase() throws IOException {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        // Путь ко вновь созданной БД
        String outFileName = DB_PATH + DB_NAME;
        // Открываем пустую базу данных как исходящий поток
        OutputStream mOutput = new FileOutputStream(outFileName);
        // Перемещаем байты из входящего файла в исходящий
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        // Закрываем потоки
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    // Проверяет, существует ли уже эта база, чтобы не копировать каждый раз при запуске приложения
    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    // Открываем БД
    void openDataBase() throws SQLException {
        String mPath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    // Используется для закрытия соединения
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Обновляем БД
        if (newVersion > oldVersion) {
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Ошибка обновления базы.");
            }
        }
    }

    // Получаем все упражнения из БД и возвращаем их в виде списка
    ArrayList<ArrayList> getAllExercise() {
        ArrayList<Integer> id_list = new ArrayList<>();
        ArrayList<String> text_list = new ArrayList<>();
        ArrayList<String> image_list = new ArrayList<>();
        ArrayList<Integer> favorite_list = new ArrayList<>();
        ArrayList<ArrayList> group = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql;

        try {
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2, favorite FROM exercise INNER JOIN exercise_favorite ON _id=exercise_id ORDER BY exercise_title ASC", null);
            if (sql == null) return null;
            while (sql.moveToNext()) {
                int id = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                int favorite = sql.getInt(sql.getColumnIndex("favorite"));
                id_list.add(id);
                text_list.add(title);
                image_list.add(image);
                favorite_list.add(favorite);
            }
        } catch (Exception e) {
            throw new Error("Не могу открыть таблицу exercise.");
        }

        sql.close();
        db.close();
        group.add(id_list);
        group.add(text_list);
        group.add(image_list);
        group.add(favorite_list);
        return group;
    }


    // Получаем все программы из БД и возвращаем их в виде списка
    ArrayList<ArrayList> getAllWorkout() {
        ArrayList<Integer> id_list = new ArrayList<>();
        ArrayList<String> text_list = new ArrayList<>();
        ArrayList<String> image_list = new ArrayList<>();
        ArrayList<ArrayList> group = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql;

        try {
            sql = db.rawQuery("SELECT _id, workout_title, workout_image FROM workout ORDER BY workout_title ASC", null);
            if (sql == null) return null;
            while (sql.moveToNext()) {
                int id = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("workout_title"));
                String image = sql.getString(sql.getColumnIndex("workout_image"));
                id_list.add(id);
                text_list.add(title);
                image_list.add(image);
            }
        } catch (Exception e) {
            throw new Error("Не могу открыть таблицу workout.");
        }

        sql.close();
        db.close();
        group.add(text_list);
        group.add(image_list);
        group.add(id_list);
        return group;
    }


    // Получаем конкретную программу из БД
    ArrayList<String> getCurrentWorkout(int id) {
        ArrayList<String> group = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql;

        try {
            sql = db.rawQuery("SELECT * FROM workout WHERE _id=?", new String[]{Integer.toString(id)});
            if (sql == null) return null;
            while (sql.moveToNext()) {
                String title = sql.getString(sql.getColumnIndex("workout_title"));
                String text = sql.getString(sql.getColumnIndex("workout_text"));
                group.add(title);
                group.add(text);
            }
        } catch (Exception e) {
            throw new Error("Не могу открыть таблицу workout.");
        }

        sql.close();
        db.close();
        return group;
    }

    // Удаляем тренировку навсегда
    void deleteWorkout(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Вначале удаляем записи из таблицы training
        db.delete("training", "workout_id=" + id, null);
        // Затем из таблицы workout
        db.delete("workout", "_id=" + id, null);
    }


    // Получаем конкретное упражнение из БД
    ArrayList<String> getCurrentExercise(int id) {
        ArrayList<String> group = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql;

        try {
            sql = db.rawQuery("SELECT * FROM exercise INNER JOIN exercise_favorite ON _id=exercise_id WHERE _id=?", new String[]{Integer.toString(id)});
            if (sql == null) return null;
            while (sql.moveToNext()) {
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String text = sql.getString(sql.getColumnIndex("exercise_text"));
                String img1 = sql.getString(sql.getColumnIndex("exercise_image"));
                String img2 = sql.getString(sql.getColumnIndex("exercise_image2"));
                String tips = sql.getString(sql.getColumnIndex("exercise_tips"));
                String favorite = sql.getString(sql.getColumnIndex("favorite"));
                group.add(title);
                group.add(text);
                group.add(img1);
                group.add(img2);
                group.add(tips);
                group.add(favorite);
            }
        } catch (Exception e) {
            throw new Error("Не могу открыть таблицу exercise.");
        }

        sql.close();
        db.close();
        return group;
    }


    ArrayList<ArrayList> getTraining(int id) {
        ArrayList<Integer> id_list = new ArrayList<>();     // Идентификатор упражнения
        ArrayList<String> image_list = new ArrayList<>();   // Картинка упражнения
        ArrayList<String> title_list = new ArrayList<>();   // Название упражнения
        ArrayList<String> set_list = new ArrayList<>();     // Подходы
        ArrayList<String> repeat_list = new ArrayList<>();  // Повторения
        ArrayList<ArrayList> group = new ArrayList<>();     // Упаковываем всё в один список для возврата
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql;

        try {
            sql = db.rawQuery("SELECT exercise_id, exercise_title, exercise_image2, training_set, training_repeat FROM training " +
                    "INNER JOIN exercise ON exercise._id = training.exercise_id " +
                    "WHERE workout_id=? ORDER BY training_order", new String[]{Integer.toString(id)});
            if (sql == null) return null;
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("exercise_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                String set = sql.getString(sql.getColumnIndex("training_set"));
                String repeat = sql.getString(sql.getColumnIndex("training_repeat"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
                set_list.add(set);
                repeat_list.add(repeat);
            }
        } catch (Exception e) {
            throw new Error("Не могу открыть таблицу training.");
        }

        sql.close();
        db.close();
        group.add(id_list);
        group.add(image_list);
        group.add(title_list);
        group.add(set_list);
        group.add(repeat_list);
        return group;
    }


    // Добавляем или удаляем упражнение из избранного
    void ExerciseFavorite(int id, int action) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Создаём объект для данных
        ContentValues cv = new ContentValues();
        // Добавляем данные для добавления
        cv.put("favorite", action);

        // Обновляем данные в таблице БД
        db.update("exercise_favorite", cv, "exercise_id = ?", new String[]{Integer.toString(id)});
        db.close();
    }


    // Создаём новую тренировку
    long newWorkout(String title, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("workout_title", title);
        values.put("workout_text", description);
        return db.insert("workout", null, values);
    }


    // Создаём новую тренировку
    void newTraining(long id, SparseArray<ArrayList> training) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0, nSize = training.size(); i < nSize; i++) {
            ContentValues values = new ContentValues();
            ArrayList exercise = training.valueAt(i);
            values.put("workout_id", id); // Идентификатор тренировки
            values.put("exercise_id", (int) exercise.get(0)); // Идентификатор упражнения
            values.put("training_set", (String) exercise.get(1)); // Повторы
            values.put("training_repeat", (String) exercise.get(2)); // Повторения
            values.put("training_order", i + 1); // Порядок упражнений

            db.insert("training", null, values);
        }
    }


    // Обновляем существующую тренировку
    void updateWorkout(int id, String title, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("workout_title", title);
        values.put("workout_text", description);

        db.update("workout", values, "_id = ?", new String[]{Integer.toString(id)});
    }


    // Обновляем упражнения для выбранной тренировки
    void updateTraining(int id, SparseArray<ArrayList> training) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Вначале удаляем существующие записи из таблицы training
        db.delete("training", "workout_id=" + id, null);

        // Затем вставляем новые записи
        for (int i = 0, nSize = training.size(); i < nSize; i++) {
            ContentValues values = new ContentValues();
            ArrayList exercise = training.valueAt(i);
            values.put("workout_id", id); // Идентификатор тренировки
            values.put("exercise_id", (int) exercise.get(0)); // Идентификатор упражнения
            values.put("training_set", (String) exercise.get(1)); // Повторы
            values.put("training_repeat", (String) exercise.get(2)); // Повторения
            values.put("training_order", i + 1); // Порядок упражнений

            db.insert("training", null, values);
        }
    }


    // Получаем упражнения для Генератора тренировок
    ArrayList<ArrayList> getExercise(int id) {
        ArrayList<Integer> id_list = new ArrayList<>();     // Идентификатор упражнения
        ArrayList<String> image_list = new ArrayList<>();   // Картинка упражнения
        ArrayList<String> title_list = new ArrayList<>();   // Название упражнения
        ArrayList<ArrayList> group = new ArrayList<>();     // Упаковываем всё в один список для возврата
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor sql = null;

        // Выбираем три случайных базовых упражнения
        if (id == 1) {
            try {
                sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                        "WHERE exercise_type=1 \n" +
                        "ORDER BY random() LIMIT 3", null);
                if (sql == null) return null;
                while (sql.moveToNext()) {
                    int eId = sql.getInt(sql.getColumnIndex("_id"));
                    String title = sql.getString(sql.getColumnIndex("exercise_title"));
                    String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                    id_list.add(eId);
                    image_list.add(image);
                    title_list.add(title);
                }
            } catch (Exception e) {
                throw new Error("Не могу открыть таблицу exercise.");
            }
        }
        // Жим штанги, изолирующее, жим гантелей
        else if (id == 2) {
            // Жим штанги
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=1 AND exercise_title LIKE '%штанг%' \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Изолирующее
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=2 AND muscle_group=2 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Жим гантелей
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=1 AND exercise_title LIKE '%гант%' \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
        }
        // Базовое, потом изолирующие на отдельные группы
        else if (id == 3) {
            // Базовое
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=1 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Изолирующее передние
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=2 AND muscle_group=1 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Изолирующее средние
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=2 AND muscle_group=2 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Изолирующее задние
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=2 AND muscle_group=3 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
        }
        // Жим гантелей, изолирующее, жим в Смите
        else if (id == 4) {
            // Гантели
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=1 AND exercise_title LIKE '%гант%' \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // Изолирующее средние
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=2 AND muscle_group=2 \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
            // В тренижёре Смита
            sql = db.rawQuery("SELECT _id, exercise_title, exercise_image2 FROM exercise \n" +
                    "WHERE exercise_type=1 AND exercise_title LIKE '%Смит%' \n" +
                    "ORDER BY random() LIMIT 1", null);
            while (sql.moveToNext()) {
                int eId = sql.getInt(sql.getColumnIndex("_id"));
                String title = sql.getString(sql.getColumnIndex("exercise_title"));
                String image = sql.getString(sql.getColumnIndex("exercise_image2"));
                id_list.add(eId);
                image_list.add(image);
                title_list.add(title);
            }
        }

        if (sql != null) sql.close();
        db.close();
        group.add(id_list);
        group.add(image_list);
        group.add(title_list);
        return group;
    }

}

