package com.programsdesigningfor.shoulders;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class GeneratorWorkout extends Fragment {
    int target = 1; // Параметры подбора тренировки
    int numWorkout = 6; // Общее число разных тренировок
    int currentWorkout = 1; // Текущая выбранная тренировка
    DataBaseHelper dbHelper;
    protected View mView;
    ArrayList<ArrayList> training; // Список с упражнениями
    String set = "4"; // Подходы по умолчанию
    String repeat = "8-10"; // Повторения по умолчанию
    String description; // Краткое описание тренировки

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_generator_workout, container, false);
        this.mView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        // Бросаем кубик и получаем номер тренировки
        currentWorkout = 1 + (int) (Math.random() * numWorkout);

        /* -------------------------------------------------
           Получаем данные пользователя
        ------------------------------------------------- */
        if (getArguments() != null) {
            target = getArguments().getInt("target", 1);
        }
        generator(target);

        /* -------------------------------------------------
           Сохраняем тренировку
        ------------------------------------------------- */
        Button saveButton = (Button) view.findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressWarnings("unchecked")
            public void onClick(View v) {
                String title = "Тенировка плеч";
                // Сохраняем тренировку в базе данных
                long lastId = dbHelper.newWorkout(title, description);

                // Переводим список training в другой формат
                SparseArray<ArrayList> workout = new SparseArray<>();
                ArrayList<Integer> exercise_id = training.get(0);

                int n = exercise_id.size();
                for (int i = 0; i < n; i++) {
                    ArrayList group = new ArrayList();
                    group.add(exercise_id.get(i));
                    group.add(set);
                    group.add(repeat);
                    workout.put(i, group);
                }

                // Сохраняем упражнения для этой тренировки
                dbHelper.newTraining(lastId, workout);

                // Выводим сообщение, что тренировка сохранена
                Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.workout_save, Toast.LENGTH_SHORT);
                toast.show();

                // Возвращаемся обратно к списку тренировок
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        /* -------------------------------------------------
           Повторяем тренировку
        ------------------------------------------------- */
        Button moreButton = (Button) view.findViewById(R.id.more);
        moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Все типы тренировок идут по кругу, друг за другом
                currentWorkout++;
                if (currentWorkout > numWorkout) currentWorkout = 1;
                // Для силовой тренировки отжимания не делаем и переходим сразу к следующей тренировке
                if (currentWorkout == 8 && target == 1) currentWorkout = 1;
                generator(target);
            }
        });
    }


    // Создаём тренировку
    @SuppressWarnings("unchecked")
    private void generator(int target) {
        // Подключаем базу данных
        dbHelper = new DataBaseHelper(getActivity());

        // В зависимости от цели ставим число повторений
        switch (target) {
            case 1:
                repeat = "4-6";
                break;
            case 2:
                repeat = "8-10";
                break;
            case 3:
                repeat = "12-15";
                break;
        }

        // Выбираем, какую тренировку ставить
        switch (currentWorkout) {
            case 1:
            default:
                // Три базовых упражнения
                training = dbHelper.getExercise(1);
                set = "4";
                description = "Тренировка делается по классической методике — на снаряде выставляется рабочий вес и выполняется заданное число повторений. Между повторами отдыхай около минуты, между упражнениями 1-3 минуты.";
                break;
            case 2:
                // Пирамида
                training = dbHelper.getExercise(1);
                set = "4";
                description = "Тренировка проходит по методу «пирамида». В первом подходе берётся лёгкий вес и делается большое число повторений. С каждым подходом рабочий вес повышается, а количество повторений понижается.";
                switch (target) {
                    case 1:
                        repeat = "8,6,4,2";
                        break;
                    case 2:
                        repeat = "12,10,8,6";
                        break;
                    case 3:
                        repeat = "15,12,10,10";
                        break;
                }
                break;
            case 3:
                // Обратная пирамида
                training = dbHelper.getExercise(1);
                set = "4";
                description = "Тренировка проходит по методу «обратной пирамиды». Предварительно выполните несколько разминочных подходов для разогрева мышц и подготовки их к работе. В первом подходе берётся тяжёлый вес и делается малое количество повторений. В последующих подходах вес снижается и увеличивается количество повторений.";
                switch (target) {
                    case 1:
                        repeat = "2,4,6,8";
                        break;
                    case 2:
                        repeat = "6,8,10,12";
                        break;
                    case 3:
                        repeat = "10,12,14,16";
                        break;
                }
                break;
            case 4:
                // Жим штанги, изолирующее, жим гантелей
                training = dbHelper.getExercise(2);
                set = "5";
                description = "Тренировка делается по классической методике — на снаряде выставляется рабочий вес и выполняется заданное число повторений. Между повторами отдыхай около минуты, между упражнениями 1-3 минуты.";
                switch (target) {
                    case 1:
                        repeat = "4-6";
                        break;
                    case 2:
                        repeat = "8-10";
                        break;
                    case 3:
                        repeat = "12-15";
                        break;
                }
                break;
            case 5:
                // Базовое и три изолирующих
                training = dbHelper.getExercise(3);
                set = "5";
                description = "Тренировка делается по классической методике — на снаряде выставляется рабочий вес и выполняется заданное число повторений. Между повторами отдыхай около минуты, между упражнениями 1-3 минуты.";
                switch (target) {
                    case 1:
                        repeat = "4-6";
                        break;
                    case 2:
                        repeat = "8-10";
                        break;
                    case 3:
                        repeat = "12-15";
                        break;
                }
                break;
            case 6:
                // Жим гантелей, изолирующее и жим в Смите
                training = dbHelper.getExercise(4);
                set = "5";
                description = "Тренировка делается по классической методике — на снаряде выставляется рабочий вес и выполняется заданное число повторений. Между повторами отдыхай около минуты, между упражнениями 1-3 минуты.";
                switch (target) {
                    case 1:
                        repeat = "4-6";
                        break;
                    case 2:
                        repeat = "8-10";
                        break;
                    case 3:
                        repeat = "12-15";
                        break;
                }
                break;
        }

        ArrayList<Integer> exercise_id = training.get(0);
        ArrayList<String> exercise_image = training.get(1);
        ArrayList<String> exercise_title = training.get(2);

        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) mView.findViewById(R.id.training_table);
        // Предварительно очищаем таблицу
        while (tableLayout.getChildCount() > 1)
            tableLayout.removeView(tableLayout.getChildAt(tableLayout.getChildCount() - 1));

        int n = exercise_id.size();
        for (int i = 0; i < n; i++) {
            addRow(exercise_image.get(i), exercise_title.get(i), set, repeat);
        }

        TextView mText = (TextView) mView.findViewById(R.id.workout_text);
        mText.setText(description);

    }

    //@SuppressWarnings({"NullableProblems"})
    public void addRow(String img, String title, String set, String repeat) {

        // Находим саму таблицу по идентификатору
        TableLayout tableLayout = (TableLayout) mView.findViewById(R.id.training_table);

        // Создаём экземпляр инфлейтера, который понадобится для создания строки таблицы из шаблона. В качестве контекста у нас используется сама активити
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        // Создаём строку таблицы, используя шаблон из файла /res/layout/row_training.xml
        final ViewGroup nullParent = null;
        TableRow tr = (TableRow) inflater.inflate(R.layout.row_training, nullParent);

        // Находим картинку для упражнения
        ImageView image_row = (ImageView) tr.findViewById(R.id.exercise_image);
        // Устанавливаем картинку
        Integer resId = getResources().getIdentifier(img, "drawable", getActivity().getPackageName());
        image_row.setImageResource(resId);

        // Находим ячейку для упражнения
        TextView exercise_row = (TextView) tr.findViewById(R.id.exercise_title);
        // Устанавливаем текст
        exercise_row.setText(title);

        // Находим ячейку для сетов
        TextView set_row = (TextView) tr.findViewById(R.id.exercise_set);
        // Устанавливаем текст
        set_row.setText(set);

        // Находим ячейку для повторений
        TextView repeat_row = (TextView) tr.findViewById(R.id.exercise_repeat);
        // Устанавливаем текст
        repeat_row.setText(repeat);

        // Добавляем созданную строку в таблицу
        tableLayout.addView(tr);
    }

}
